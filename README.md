gsetri
======

Grenada Solar Energy Technology Research Institute

Generator-jekyllrb wraps the Jekyll static site generator in a Yeoman development workflow. Scaffold your site with Yo, manage front end packages with Bower, and automate development and build tasks with Grunt.

Generator-jekyllrb is ideal for developing performant static sites and prototyping dynamic sites and apps (especially if the final version uses Yeoman too). It's also a great introduction to Yeoman if you're not familiar with JavaScript MV* frameworks.
